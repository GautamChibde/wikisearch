package com.chibde.wikisearch.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.chibde.wikisearch.R
import com.chibde.wikisearch.di.Injectable
import com.chibde.wikisearch.testing.OpenForTesting
import com.chibde.wikisearch.ui.adapter.ImageAdapter
import com.chibde.wikisearch.ui.viewmodel.ImageItem
import com.chibde.wikisearch.ui.viewmodel.WikiResultViewModel
import com.chibde.wikisearch.utils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_search.*
import javax.inject.Inject

@OpenForTesting
class SearchFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: WikiResultViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(WikiResultViewModel::class.java)

        search_fragment_rv_page.layoutManager = GridLayoutManager(context, 4)
        search_fragment_rv_page.adapter = ImageAdapter(imageSelected = { imageSelected(it) })

        viewModel.images.observe(this, Observer { imageUrls ->
            fragment_search_progress.visibility = View.INVISIBLE
            (search_fragment_rv_page.adapter as ImageAdapter).refresh(imageUrls)
        })

        viewModel.error.observe(this, Observer { error ->
            error?.let {
                fragment_search_tv_error.visibility = View.VISIBLE
                fragment_search_tv_error.text = error
                (search_fragment_rv_page.adapter as ImageAdapter).refresh(listOf())
                fragment_search_progress.visibility = View.INVISIBLE
            }
        })

        fragment_search_et_search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchQuery(fragment_search_et_search.text.toString())
                    hideKeyboard()
                }
                return false
            }
        })
    }

    private fun imageSelected(imageItem: ImageItem) {
        val action = SearchFragmentDirections.showImage(imageItem.imageUrl, imageItem.title)
        findNavController().navigate(action)
    }

    private fun searchQuery(query: String) {
        (search_fragment_rv_page.adapter as ImageAdapter).refresh(listOf())
        fragment_search_tv_error.visibility = View.GONE
        fragment_search_progress.visibility = View.VISIBLE
        viewModel.searchImages(query)
    }
}
