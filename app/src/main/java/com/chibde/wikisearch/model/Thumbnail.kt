package com.chibde.wikisearch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Thumbnail(
    @JsonProperty("height") val height: Int?,
    @JsonProperty("source") val source: String?,
    @JsonProperty("width") val width: Int?
)