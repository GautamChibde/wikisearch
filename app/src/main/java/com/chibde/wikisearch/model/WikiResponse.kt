package com.chibde.wikisearch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class WikiResponse(
    @JsonProperty("batchcomplete") val batchComplete: String?,
    @JsonProperty("query") val query: Query?
)