package com.chibde.wikisearch.api.exceptions

import java.io.IOException

sealed class NetworkException: IOException()

class NoConnectivityException : NetworkException()
class BadRequestException(override var message: String) : NetworkException()
class InternalServerException(override var message: String) : NetworkException()
