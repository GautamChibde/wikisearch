package com.chibde.wikisearch.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

@RunWith(JUnit4::class)
class WikiApiServiceTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: WikiApiService


    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        val objectMapper = ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(WikiApiService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test not null`() {

        val wikiApi = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(WikiApiService::class.java)
        MatcherAssert.assertThat(wikiApi, CoreMatchers.notNullValue())
    }

    @Test
    fun `test get wiki api data`() = runBlocking {
        enqueueResponse("wiki_search_data.json")
        val result = service.getImagesAsync(
            searchTerm = "",
            imageSize = 100
        ).await()

        MatcherAssert.assertThat(result, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(result.query, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(result.query?.pages?.size, CoreMatchers.`is`(8))

        val pageWithEmptyThumbnail = result.query?.pages?.filter {
            it.thumbnail == null
        }?.count()

        MatcherAssert.assertThat(pageWithEmptyThumbnail, CoreMatchers.`is`(0))

        val firstPage = result.query?.pages?.first()

        MatcherAssert.assertThat(firstPage, CoreMatchers.notNullValue())

        MatcherAssert.assertThat(firstPage?.title, CoreMatchers.`is`("Test (assessment)"))
        MatcherAssert.assertThat(
            firstPage?.thumbnail?.source,
            CoreMatchers.`is`("https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Donboscocambodia0001.JPG/100px-Donboscocambodia0001.JPG")
        )
    }

    private fun enqueueResponse(fileName: String) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream(fileName)
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        mockWebServer.enqueue(
            mockResponse
                .setBody(source.readString(Charsets.UTF_8))
        )
    }
}