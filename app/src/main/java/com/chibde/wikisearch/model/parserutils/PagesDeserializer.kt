package com.chibde.wikisearch.model.parserutils

import com.chibde.wikisearch.model.Page
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

class PagesDeserializer : JsonDeserializer<List<Page>>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): List<Page> {
        return p.codec
            .readTree<JsonNode>(p)
            .map {
                ObjectMapper().treeToValue(it, Page::class.java)
            }.filter {
                it.thumbnail != null
            }.toList()
    }
}