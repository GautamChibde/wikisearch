package com.chibde.wikisearch.repository

import com.chibde.wikisearch.api.WikiApiService
import com.chibde.wikisearch.api.exceptions.BadRequestException
import com.chibde.wikisearch.api.exceptions.InternalServerException
import com.chibde.wikisearch.api.exceptions.NetworkException
import com.chibde.wikisearch.api.exceptions.NoConnectivityException
import com.chibde.wikisearch.model.Page
import com.chibde.wikisearch.testing.OpenForTesting
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class WikiRepository @Inject constructor(
    private val wikiApiService: WikiApiService
) {

    suspend fun getPages(searchTerm: String, imageSize: Int = 200): WikiDataResults {
        try {
            val wikiResponse = wikiApiService.getImagesAsync(
                searchTerm = searchTerm,
                imageSize = imageSize
            ).await()

            wikiResponse.query?.let {
                return WikiDataResults(
                    success = true,
                    data = it.pages
                )
            } ?: run {
                return WikiDataResults(
                    success = true,
                    data = listOf()
                )
            }

        } catch (e: NoConnectivityException) {
            return WikiDataResults(
                success = false,
                error = NoConnectivityException(),
                message = "Please Check you internet connection"
            )
        } catch (e: BadRequestException) {
            return WikiDataResults(
                success = false,
                error = BadRequestException(e.message),
                message = "Please Check you internet connection"

            )
        } catch (e: InternalServerException) {
            return WikiDataResults(
                success = false,
                error = InternalServerException(e.message),
                message = "Something went wrong on our side please try again"
            )
        }
    }
}

data class WikiDataResults(
    val success: Boolean,
    val error: NetworkException? = null,
    val message: String = "",
    val data: List<Page>? = null
)