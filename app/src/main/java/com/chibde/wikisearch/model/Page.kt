package com.chibde.wikisearch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Page(
    @JsonProperty("index") val index: Int?,
    @JsonProperty("ns") val ns: Int?,
    @JsonProperty("pageid") val pageId: Int?,
    @JsonProperty("thumbnail") val thumbnail: Thumbnail?,
    @JsonProperty("title") val title: String?
)