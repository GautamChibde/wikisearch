package com.chibde.wikisearch.ui.fragment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.chibde.wikisearch.R
import com.chibde.wikisearch.testing.SingleFragmentActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ImageFragmentTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, true)

    private val title = "Title"

    private val imageFragment = TestImageFragment().apply {
        arguments = ImageFragmentArgs.Builder(
                "url",
                title)
                .build()
                .toBundle()
    }

    @Before
    fun init() {
        activityRule.activity.setFragment(imageFragment)
    }

    @Test
    fun testDataLoaded() {
        onView(withId(R.id.fragment_image_tv_title))
            .check(matches(withText(title)))

        onView(withId(R.id.fragment_image_iv))
            .check(matches(isDisplayed()))
    }

    class TestImageFragment: ImageFragment()
}