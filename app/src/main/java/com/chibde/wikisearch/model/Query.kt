package com.chibde.wikisearch.model

import com.chibde.wikisearch.model.parserutils.PagesDeserializer
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

data class Query(
    @JsonDeserialize(using = PagesDeserializer::class)
    @JsonProperty("pages")
    val pages: List<Page> = listOf()
)