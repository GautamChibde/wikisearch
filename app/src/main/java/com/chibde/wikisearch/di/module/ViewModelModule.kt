package com.chibde.wikisearch.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chibde.wikisearch.di.ViewModelKey
import com.chibde.wikisearch.ui.viewmodel.WikiResultViewModel
import com.chibde.wikisearch.utils.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@SuppressWarnings("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(WikiResultViewModel::class)
    abstract fun bindWikiResultViewModel(wikiResultViewModel: WikiResultViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

}