package com.chibde.wikisearch.testing

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
