package com.chibde.wikisearch.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.chibde.wikisearch.R
import com.chibde.wikisearch.testing.OpenForTesting
import kotlinx.android.synthetic.main.fragment_image.*

@OpenForTesting
class ImageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.also {
            val imageUrl = ImageFragmentArgs.fromBundle(it).imageUrl
            val title = ImageFragmentArgs.fromBundle(it).title

            fragment_image_tv_title.text = title

            Glide.with(fragment_image_iv.context).setDefaultRequestOptions(
                RequestOptions()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.noimage)
            ).load(imageUrl)
                .into(fragment_image_iv)
        }

        fragment_image_ib_back.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
