package com.chibde.wikisearch.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chibde.wikisearch.api.WikiApiService
import com.chibde.wikisearch.util.TestUtil
import com.nhaarman.mockitokotlin2.doAnswer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

@RunWith(JUnit4::class)
class WikiRepositoryTest {
    private lateinit var repository: WikiRepository
    private val service = Mockito.mock(WikiApiService::class.java)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        repository = WikiRepository(service)
    }

    @Test
    fun getPagesTest() = runBlocking {
        // Given
        val wikiResponse = TestUtil.createDummyWikiResponse()

        // When
        Mockito.`when`(
            service.getImagesAsync(
                searchTerm = "",
                imageSize = 200
            )
        ).doAnswer {
            GlobalScope.async {
                wikiResponse
            }
        }

        // Do
        val wikiResults = async { repository.getPages(searchTerm = "") }.await()

        // Then
        MatcherAssert.assertThat(
            wikiResults, CoreMatchers.`is`(
                WikiDataResults(
                    success = true,
                    message = "",
                    data = wikiResponse.query?.pages
                )
            )
        )
    }
}