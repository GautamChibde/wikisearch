package com.chibde.wikisearch.api

import com.chibde.wikisearch.model.WikiResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface WikiApiService {

    @GET("api.php")
    fun getImagesAsync(
        @Query("gpssearch") searchTerm: String,
        @Query("pithumbsize") imageSize: Int,
        @Query("action") action: String = "query",
        @Query("prop") prop: String = "pageimages",
        @Query("format") format: String = "json",
        @Query("piprop") piprop: String = "thumbnail",
        @Query("pilimit") piLimit: Int = 50,
        @Query("generator") generator: String = "prefixsearch"
    ): Deferred<WikiResponse>
}