# Wikipedia Search APP

Simple Android App written in kotlin that searches images based and query on wikipedia.

Project follows MVVM architecture.

#### App uses following libraries
* Dagger 2 - Dependency injection
* Retrofit - HTTP client for Android
* Jackson - Json Parser
* Koltin-coroutines
* [kotlin-allopen](https://kotlinlang.org/docs/reference/compiler-plugins.html)
* [Architecture Components](https://developer.android.com/arch)
    * [ViewModels](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
    * [Navigation](https://developer.android.com/guide/navigation)