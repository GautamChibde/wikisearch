package com.chibde.wikisearch.util

import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.chibde.wikisearch.model.Page
import com.chibde.wikisearch.model.Query
import com.chibde.wikisearch.model.Thumbnail
import com.chibde.wikisearch.model.WikiResponse
import com.chibde.wikisearch.ui.viewmodel.ImageItem
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object TestUtil {

    fun createDummyWikiResponse(): WikiResponse {
        return WikiResponse(
            batchComplete = "",
            query = Query(
                listOf(
                    Page(
                        index = 1,
                        ns = 1,
                        title = "Test Title 1",
                        pageId = 1,
                        thumbnail = Thumbnail(
                            height = 200,
                            width = 200,
                            source = "http://www.test1.com"
                        )
                    ),
                    Page(
                        index = 2,
                        ns = 2,
                        title = "Test Title 2",
                        pageId = 2,
                        thumbnail = Thumbnail(
                            height = 200,
                            width = 200,
                            source = "http://www.test2.com"
                        )
                    )
                )
            )
        )
    }

    fun createDummyImageUrls(): List<String> {
        return listOf(
            "http://goo.gl/gEgYUd",
            "http://goo.gl/gEgYUd",
            "http://goo.gl/gEgYUd"
        )
    }

    fun createDummyImageItems(): List<ImageItem> {
        return listOf(
            ImageItem(
                imageUrl = "http://goo.gl/gEgYUd",
                title = "title1"
            ),
            ImageItem(
                imageUrl = "http://goo.gl/gEgYUd",
                title = "title2"
            ),
            ImageItem(
                imageUrl = "http://goo.gl/gEgYUd",
                title = "title3"
            )
        )
    }
}

@Throws(InterruptedException::class)
fun <T> LiveData<T>.getValueSync(): T {
    val data = arrayOfNulls<Any>(1)
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(@Nullable o: T) {
            data[0] = o
            latch.countDown()
            removeObserver(this)
        }
    }
    this.observeForever(observer)
    latch.await(2, TimeUnit.SECONDS)

    return data[0] as T
}