package com.chibde.wikisearch.di.module

import com.chibde.wikisearch.ui.fragment.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributesSearchFragment(): SearchFragment
}