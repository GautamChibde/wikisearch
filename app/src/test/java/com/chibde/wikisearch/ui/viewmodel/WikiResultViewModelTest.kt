package com.chibde.wikisearch.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.chibde.wikisearch.repository.WikiDataResults
import com.chibde.wikisearch.repository.WikiRepository
import com.chibde.wikisearch.util.TestUtil
import com.chibde.wikisearch.util.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

@RunWith(JUnit4::class)
class WikiResultViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository = Mockito.mock(WikiRepository::class.java)
    private lateinit var viewModel: WikiResultViewModel

    @Before
    fun init() {
        viewModel = WikiResultViewModel(repository)
    }
    @Test
    fun searchImages() = runBlocking {
        val pages = TestUtil.createDummyWikiResponse().query!!.pages
        val dataResults = WikiDataResults(
            success = true,
            data = pages
        )
        whenever(repository.getPages(searchTerm = "")).thenReturn(dataResults)
        val observer = mock<Observer<List<ImageItem>>>()
        viewModel.images.observeForever(observer)
        withContext(Dispatchers.Default) {
            viewModel.searchImages(searchTerm = "")
            delay(100)
        }

        verify(observer).onChanged(WikiResultViewModel.getImageItems(pages))
    }


    @Test
    fun testErrorResponse() = runBlocking {
        val message = "Error has occurred"
        val dataResults = WikiDataResults(
            success = false,
            message = message,
            data = null
        )
        whenever(repository.getPages(searchTerm = "")).thenReturn(dataResults)

        val observer = mock<Observer<String>>()
        viewModel.error.observeForever(observer)

        withContext(Dispatchers.Default) {
            viewModel.searchImages(searchTerm = "")
            delay(100)
        }

        verify(observer).onChanged(message)
    }
}