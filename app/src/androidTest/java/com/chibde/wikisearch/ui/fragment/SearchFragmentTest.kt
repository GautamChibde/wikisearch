package com.chibde.wikisearch.ui.fragment

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.pressImeActionButton
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.chibde.wikisearch.R
import com.chibde.wikisearch.testing.SingleFragmentActivity
import com.chibde.wikisearch.ui.viewmodel.ImageItem
import com.chibde.wikisearch.ui.viewmodel.WikiResultViewModel
import com.chibde.wikisearch.util.TestUtil
import com.chibde.wikisearch.utils.RecyclerViewItemCountAssertion
import com.chibde.wikisearch.utils.ViewModelUtil
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.`when`

@RunWith(AndroidJUnit4::class)
class SearchFragmentTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, true)
    private lateinit var viewModel: WikiResultViewModel

    private val error = MutableLiveData<String>()

    private val imageUrls = MutableLiveData<List<ImageItem>>()

    private val searchFragment = TestSearchFragment()

    @Before
    fun init() {
        viewModel = Mockito.mock(WikiResultViewModel::class.java)
        `when`(viewModel.images).thenReturn(imageUrls)
        `when`(viewModel.error).thenReturn(error)
        searchFragment.viewModelFactory = ViewModelUtil.createFor(viewModel)
        activityRule.activity.setFragment(searchFragment)
    }

    @Test
    fun testFirstLoad() {
        onView(
            withId(R.id.fragment_search_progress))
            .check(
                ViewAssertions.matches(
                not(ViewMatchers.isDisplayed())))

        onView(ViewMatchers.withId(R.id.fragment_search_tv_error))
            .check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))

        onView(ViewMatchers.withId(R.id.fragment_search_et_search))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testProgressShown() {
        onView(withId(R.id.fragment_search_et_search)).perform(typeText("something\n"))

        onView(
            withId(R.id.fragment_search_et_search)
        ).perform(pressImeActionButton())

        onView(
            withId(R.id.fragment_search_progress))
            .check(
                ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView((withId(R.id.search_fragment_rv_page))).check(
            RecyclerViewItemCountAssertion(0)
        )
    }

    @Test
    fun testDisplayedResults() {
        val images = TestUtil.createDummyImageItems()
        imageUrls.postValue(images)

        onView(
            withId(R.id.fragment_search_progress))
            .check(
                ViewAssertions.matches(
                    not(ViewMatchers.isDisplayed())))

        onView(ViewMatchers.withId(R.id.fragment_search_tv_error))
            .check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))

        onView((withId(R.id.search_fragment_rv_page))).check(
            RecyclerViewItemCountAssertion(images.size)
        )
    }

    @Test
    fun testEmptyResults() {
        val errorMessage = "Error"
        error.postValue(errorMessage)

        onView(ViewMatchers.withId(R.id.fragment_search_tv_error))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.fragment_search_tv_error))
            .check(ViewAssertions.matches(withText(errorMessage)))
    }

    class TestSearchFragment: SearchFragment()
}