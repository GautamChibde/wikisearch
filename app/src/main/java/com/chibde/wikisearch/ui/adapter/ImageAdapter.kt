package com.chibde.wikisearch.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.chibde.wikisearch.R
import com.chibde.wikisearch.ui.viewmodel.ImageItem
import kotlinx.android.synthetic.main.item_image.view.*

class ImageAdapter(
    private val items: MutableList<ImageItem> = mutableListOf(),
    private val imageSelected: (url: ImageItem) -> Unit
): RecyclerView.Adapter<ImageAdapter.ImageHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        return ImageHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_image, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        with(holder.itemView) {
            Glide.with(context).setDefaultRequestOptions(
                RequestOptions()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.noimage)
                    .override(item_image_iv_photo.width, item_image_iv_photo.height)
            ).load(items[position].imageUrl)
                .into(item_image_iv_photo)

            setOnClickListener {
                imageSelected.invoke(items[position])
            }
        }
    }

    fun refresh(imageUrls: List<ImageItem>) {
        items.clear()
        items.addAll(imageUrls)
        notifyDataSetChanged()
    }

    class ImageHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}