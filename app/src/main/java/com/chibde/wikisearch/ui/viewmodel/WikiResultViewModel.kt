package com.chibde.wikisearch.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chibde.wikisearch.model.Page
import com.chibde.wikisearch.repository.WikiRepository
import com.chibde.wikisearch.testing.OpenForTesting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@OpenForTesting
class WikiResultViewModel @Inject constructor(
    private val repository: WikiRepository
) : ViewModel() {

    private val _images = MutableLiveData<List<ImageItem>>()
    val images: LiveData<List<ImageItem>>
        get() = _images

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    fun searchImages(searchTerm: String) {
        GlobalScope.launch(Dispatchers.IO) {
            val wikiResults = repository.getPages(
                searchTerm = searchTerm
            )
            if (wikiResults.success) {
                wikiResults.data?.let { pages ->
                    if (pages.isEmpty()) {
                        _error.postValue("No Results")
                    } else {
                        getImageItems(pages).also { imageItems ->
                            _images.postValue(imageItems)
                            _error.postValue(null)
                        }
                    }
                }
            } else {
                _error.postValue(wikiResults.message)
            }
        }
    }

    companion object {
        fun getImageItems(pages: List<Page>): List<ImageItem> {
            return pages.map {
                ImageItem(
                    title = it.title ?: "",
                    imageUrl = it.thumbnail?.source ?: ""
                )
            }
        }
    }
}

data class ImageItem(
    val imageUrl: String,
    val title: String
)