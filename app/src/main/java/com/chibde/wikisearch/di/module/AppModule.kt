package com.chibde.wikisearch.di.module

import android.app.Application
import com.chibde.wikisearch.api.ConnectivityInterceptor
import com.chibde.wikisearch.api.RequestInterceptor
import com.chibde.wikisearch.api.WikiApiService
import com.chibde.wikisearch.utils.Constants
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun getConnectivityInterceptor(app: Application): ConnectivityInterceptor {
        return ConnectivityInterceptor(app.applicationContext)
    }

    @Singleton
    @Provides
    fun getRequestInterceptor(): RequestInterceptor {
        return RequestInterceptor()
    }

    @Singleton
    @Provides
    fun getLogginInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Singleton
    @Provides
    fun getOkHttpClient(
        connectivityInterceptor: ConnectivityInterceptor,
        requestInterceptor: RequestInterceptor,
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(requestInterceptor)
            .addInterceptor(connectivityInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun getObjectMapper(): ObjectMapper {
        return ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    @Singleton
    @Provides
    fun getRetrofitService(
        okHttpClient: OkHttpClient,
        objectMapper: ObjectMapper
    ): WikiApiService {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .build()
            .create(WikiApiService::class.java)
    }
}